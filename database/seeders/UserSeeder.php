<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            $permissions = [
                'user.view',
                'user.create',
                'user.update',
                'user.delete',
                'permission.view',
                'permission.create',
                'permission.update',
                'permission.delete',
                'role.view',
                'role.create',
                'role.update',
                'role.delete'
            ];
            foreach ($permissions as $permission) {
                Permission::create(['name' => $permission]);
            }
            $roles = [
                'super-admin'
            ];
            foreach ($roles as $role) {
                Role::create(['name' => $role]);
            }
            $role_admin = Role::whereName('super-admin')->first();
            $role_admin->givePermissionTo($permissions);
            $user = User::create([
                'name' => 'Administrator',
                'email' => 'admin@gmail.com',
                'type' => 'super-admin',
                'password' => Hash::make('admin123'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            $user->assignRole('super-admin');
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex->getMessage());
        }
    }
}
