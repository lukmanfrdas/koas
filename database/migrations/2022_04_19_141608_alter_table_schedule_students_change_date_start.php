<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableScheduleStudentsChangeDateStart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule_students', function (Blueprint $table) {
            $table->date('date_start')->change();
            $table->date('date_end')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule_students', function (Blueprint $table) {
            $table->integer('date_start')->change();
            $table->integer('date_end')->change();
        });
    }
}
