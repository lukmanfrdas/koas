<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLevelIdToStasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stases', function (Blueprint $table) {
            $table->integer('level_id')->after('description');
            $table->integer('week')->after('level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stases', function (Blueprint $table) {
            $table->integer('level_id')->after('description');
            $table->integer('week')->after('level_id');
        });
    }
}
