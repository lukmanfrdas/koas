<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class QueryAddPermissionQuotaToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
        public function up()
        {
            $permissions = [
                'quota.view',
                'quota.create',
                'quota.update',
                'quota.delete',
            ];
            foreach ($permissions as $permission) Permission::updateOrCreate(['name' => $permission]);

            $role_admin = Role::whereName('super-admin')->first();
            $role_admin->givePermissionTo($permissions);
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            $permissions = [
                'quota.view',
                'quota.create',
                'quota.update',
                'quota.delete',
            ];
            foreach ($permissions as $permission) Permission::updateOrCreate(['name' => $permission]);

            $role_admin = Role::whereName('super-admin')->first();
            $role_admin->revokePermissionTo($permissions);
        }
}
