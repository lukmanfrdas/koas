<?php

return [
    'type' => [
        1 => 'super-admin',
    ],
    'balance' => [
        0 => 'Pending',
        1 => 'Success',
        99 => 'Reject',
        999 => 'Failed'
    ],

];
