<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ route('home') }}" aria-expanded="false">
                        <i class="icon-Car-Wheel"></i>
                        <span class="hide-menu">Dashboard </span>
                    </a>
                </li>

                @can('user.view', 'permission.view', 'role.view')
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
                            aria-expanded="false">
                            <i class="fas fa-user-circle"></i>
                            <span class="hide-menu">User </span>
                        </a>
                        <ul aria-expanded="false" class="collapse first-level">
                            @can('user.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('user.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-email"></i>
                                        <span class="hide-menu"> User List </span>
                                    </a>
                                </li>
                            @endcan

                            @can('permission.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('permission.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-adjust"></i><span class="hide-menu">User Permission</span>
                                    </a>
                                </li>
                            @endcan

                            @can('role.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('role.index') }}" class="sidebar-link"><i
                                            class="mdi mdi-adjust"></i><span class="hide-menu">User Role</span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('balance.view')
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
                            aria-expanded="false"><i class="mdi mdi-cash-multiple"></i><span
                                class="hide-menu">Keuangan</span></a>

                        <ul aria-expanded="false" class="collapse first-level">
                            @can('balance.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('balance.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-cash-multiple"></i>
                                        <span class="hide-menu"> Riwayat Saldo</span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('category.view')
                <li class="sidebar-item">
                    <a href="{{ route('category.index') }}" class="sidebar-link">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu"> Category </span>
                    </a>
                </li>
                @endcan

                @can('student.view')
                <li class="sidebar-item">
                    <a href="{{ route('student.index') }}" class="sidebar-link ml-1">
                        <i class="fas fa-user-plus"></i>
                        <span class="hide-menu"> Mahasiswa </span>
                    </a>
                </li>
                @endcan

                @can('hospital.view')
                <li class="sidebar-item">
                    <a href="{{ route('hospital.index') }}" class="sidebar-link">
                        <i class="fas fa-notes-medical"></i>
                        <span class="hide-menu"> Rumah Sakit </span>
                    </a>
                </li>
                @endcan

                @can('stase.view')
                <li class="sidebar-item">
                    <a href="{{ route('stase.index') }}" class="sidebar-link">
                        <i class="fas fa-sticky-note"></i>
                        <span class="hide-menu"> Stase </span>
                    </a>
                </li>
                @endcan
                @can('level.view')
                <li class="sidebar-item">
                    <a href="{{ route('level.index') }}" class="sidebar-link">
                        <i class="fas fa-sort-numeric-up"></i>
                        <span class="hide-menu"> Semester </span>
                    </a>
                </li>
                @endcan
                @can('grade.view')
                <li class="sidebar-item">
                    <a href="{{ route('grade.index') }}" class="sidebar-link">
                        <i class="fas fa-sticky-note"></i>
                        <span class="hide-menu"> Angkatan </span>
                    </a>
                </li>
                @endcan
                @can('schedule.view')
                <li class="sidebar-item">
                    <a href="{{ route('schedule.index') }}" class="sidebar-link">
                        <i class="fas fa-calendar-alt"></i>
                        <span class="hide-menu"> Schedule </span>
                    </a>
                </li>
                @endcan
            </ul>
        </nav>
    </div>
</aside>
