@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">Semester</h4>
                                <h6 class="card-subtitle">Detail Semester</h6>
                            </div>
                            <div class="col-md-4" align="right">
                                <a href="{{ route($module . '.index') }}" class="btn btn-danger btn-lg"><i
                                        class="m-r-10 mdi mdi-backspace"></i>Kembali</a>
                            </div>
                        </div>
                        <hr>
                        <div class="form form-horizontal">
                            @foreach ($shows as $item)
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{ ucfirst($item) }}</label>
                                    <div class="col-sm-10">
                                        {{ $detail->{$item} }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title"> Schedules in {{ 'Semester' . ' ' . $detail->name}}</h4>
                                <h6 class="card-subtitle">Detail Data Schedules {{ 'Semester' }}</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="form form-horizontal">
                            <table class="table table-striped table-hover table-bordered data-table-schedules">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Date Start</th>
                                        <th>Date End</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script src="{{ asset('assets/libs/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/libs/morris.js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $('.data-table-schedules').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route($module . '.show', $detail->id) }}',
            columns: [{
                    data: 'code',
                    name: 'code'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'date_start',
                    name: 'date_start'
                },
                {
                    data: 'date_end',
                    name: 'date_end'
                },
                {
                    data: 'action',
                    name: 'action',
                    "searchable": false
                }
            ]
        });
    </script>
@endpush
