@extends('layouts.app-without-sidebar')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="d-md-flex flex-row justify-content-between align-items-center w-100">
                                <div>
                                    <h4 class="card-title">Rumah Sakit</h4>
                                    <h6 class="card-subtitle">Detail Rumah Sakit</h6>
                                </div>
                                <div id="buttons" class="my-md-0 my-2 d-flex flex-row">
                                    <div class="mr-2" align="right">
                                        <a href="{{ route('schedules.export', $id) }}" class="btn btn-primary btn-lg"><i
                                                class="fa fa-download"></i> Export</a>
                                    </div>
                                    <button type="button" class="btn btn-primary mr-2" data-toggle="modal" data-target="#filterModal">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter
                                    </button>
                                    {!! $buttons !!}
                                    <a href="{{ route($module . '.show', $id) }}" class="btn btn-danger btn-lg ml-2"><i
                                        class="m-r-10 mdi mdi-backspace"></i>Kembali</a>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="d-none" id="loading-el">
                                    Loading...
                                </div>
                                <div class="gantt_control mb-2 text-center">
                                    <button class='zoom_toggle btn btn-sm btn-primary' onclick="toggleMode(this)">Zoom to
                                        Fit</button>
                                    <input class='btn btn-sm btn-primary' type=button value="Zoom In" onclick="zoom_in();">
                                    <input class='btn btn-sm btn-primary' type=button value="Zoom Out"
                                        onclick="zoom_out();">
                                </div>

                                <div id="gantt_here" style='height:80vh;'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="filterModalLabel">Filter Jadwal</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="col-12">
                    <div class="group">
                        <label for="">Rumah Sakit</label>
                        <select name="hospital_id" id="hospital-select" class="form-control">
                            <option value="all" selected>Semua</option>
                            @foreach ($hospitals as $hospital)
                                <option value="{{ $hospital->id }}">{{ $hospital->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="group my-3">
                        <label for="">Stase</label>
                        <select name="stase_id" id="stase-select" class="form-control">
                            <option value="all" selected>Semua</option>
                            @foreach ($stases as $stase)
                                <option value="{{ $stase->id }}">{{ $stase->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="group">
                        <label for="">Cari Mahasiswa/i</label>
                        <input type="text" id="student-search" placeholder="Cari Mahasiswa.." class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="button" class="btn btn-primary" onclick="handleFilter()">Terapkan</button>
            </div>
          </div>
        </div>
    </div>
@stop

@include('pages.schedule.gantt')
