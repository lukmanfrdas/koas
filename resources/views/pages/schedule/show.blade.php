@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">Rumah Sakit</h4>
                                <h6 class="card-subtitle">Detail Rumah Sakit</h6>
                            </div>
                            <div class="col-md-4" align="right">
                                <a href="{{ route($module . '.index') }}" class="btn btn-danger btn-lg"><i
                                        class="m-r-10 mdi mdi-backspace"></i>Kembali</a>
                            </div>
                        </div>
                        <hr>
                        {{-- <div class="form form-horizontal">
                            @foreach ($shows as $item)
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">{{ ucfirst($item) }}</label>
                                    <div class="col-sm-10">
                                        {{ $detail->{$item} }}
                                    </div>
                                </div>
                            @endforeach
                        </div> --}}
                        <div class="form form-horizontal">
                            <table class="table table-responsive table-bordered">
                                <tr>
                                    <th></th>
                                    @foreach ($stases as $stase)
                                        <th>{{ $stase->name }}</th>
                                    @endforeach
                                </tr>
                                @foreach ($hospitals as $hospital)
                                    <tr>
                                        <th>{{ $hospital->name }}</th>
                                        @foreach ($stases as $stase)
                                            <td>{{ $dashboard[$hospital->id][$stase->id]['student'] }}
                                                ({{ $dashboard[$hospital->id][$stase->id]['quota'] }})
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </table>
                            {{-- <table class="table table-responsive table-bordered">
                                <tr>
                                    <th>Kelompok</th>
                                    <th>Nama Mahasiswa</th>
                                    @for ($i = 1; $i <= $total_week; $i++)
                                    <th>{{ date('d M', strtotime("+" . $i. " week", strtotime($detail->date_start))) }}</th>
                                    @endfor
                                </tr>
                                @foreach ($schedules as $k => $schedule)
                                    @foreach ($schedule as $student_id => $value)
                                    <tr>

                                        <td>{{ $k }}</td>
                                        <td>{{ $value['student']->name }}</td>
                                        @if (!empty($value['schedule']))
                                            @foreach ($value['schedule'] as $schedule)
                                            <td colspan="{{ $schedule->stase->week }}" bgcolor="{{ $schedule->hospital->color }}">{{ $schedule->stase->name }} ({{ $schedule->hospital->name }})</td>
                                            @endforeach
                                        @endif

                                    </tr>
                                    @endforeach
                                @endforeach

                            </table> --}}
                            <div class="gantt">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="gantt_control mb-2 text-center">
                    <button class='zoom_toggle btn btn-sm btn-primary' onclick="toggleMode(this)">Zoom to Fit</button>
                    <input class='btn btn-sm btn-primary' type=button value="Zoom In" onclick="zoom_in();">
                    <input class='btn btn-sm btn-primary' type=button value="Zoom Out" onclick="zoom_out();">
                    <a href="{{ route($module . '.fullscreen', $id) }}" class='btn btn-sm btn-primary' target="_blank">Fullscreen</a>
                </div>

                <div id="gantt_here" style='height:400px;'></div>
            </div>
        </div>
    </div>
@stop

@include('pages.schedule.gantt')
