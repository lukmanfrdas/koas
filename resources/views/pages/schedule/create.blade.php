@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">Jadwal</h4>
                                <h6 class="card-subtitle">Pengelolaan Jadwal</h6>
                            </div>
                            <div class="col-md-4" align="right">
                                <a href="{{ route($module . '.index') }}" class="btn btn-danger btn-lg"><i
                                        class="m-r-10 mdi mdi-backspace"></i>Kembali</a>
                            </div>
                        </div>
                        <hr>
                        <div class="form">
                            <form method="POST" action="{{ route($module . '.select-student') }}" accept-charset="UTF-8"
                                novalidate="novalidate">
                                @csrf
                                <div class="form-group">
                                    <label for="code" class="control-label">Code</label>
                                    <input class="form-control" name="code" type="text" id="code">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">Nama</label>
                                    <input class="form-control" name="name" type="text" id="name">
                                </div>
                                <div class="form-group">
                                    <label for="description" class="control-label">Deskripsi</label>
                                    <textarea class="form-control" name="description" cols="50" rows="10" id="description"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="date_start" class="control-label">Tanggal Mulai</label>
                                    <input class="form-control" name="date_start" type="date" id="date_start">
                                </div>
                                <div class="form-group">
                                    <label for="date_end" class="control-label">Tanggal Berakhir</label>
                                    <input class="form-control" name="date_end" type="date" id="date_end">
                                </div>
                                <div class="form-group">
                                    <label for="code" class="control-label">Jumlah Orang Per Kelompok</label>
                                    <input class="form-control" name="group_content" type="text" id="code">
                                </div>
                                <div class="form-group">
                                    <label for="date_end" class="control-label">Pilih Angkatan</label>
                                    <select name="grade_id" class="form-control">
                                        <option> - Pilih Angkatan - </option>
                                        @foreach ($grades as $grade)
                                        <option value="{{ $grade->id }}">{{ $grade->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="date_end" class="control-label">Pilih Semester</label>
                                    <select name="level_id" class="form-control">
                                        <option> - Pilih Semester - </option>
                                        @foreach ($levels as $level)
                                        <option value="{{ $level->id }}">{{ $level->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <button class="btn btn-success" type="submit">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
