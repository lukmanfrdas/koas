@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">Jadwal</h4>
                                <h6 class="card-subtitle">Pengelolaan Jadwal</h6>
                            </div>
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>Group</th>
                                        <th>Nama</th>
                                        <th>Tahun Ajaran</th>
                                        <th>Mulai</th>
                                        <th>Akhir</th>
                                        <th>Nilai Keseluruhan</th>
                                        <th>Nilai Lulus</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script>
        $(document).ready(function() {
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route($module . '.report', $id) }}',
                columns: [{
                        data: 'group',
                        name: 'group'
                    },
                    {
                        data: 'student.name',
                        name: 'student.name'
                    },
                    {
                        data: 'school_year',
                        name: 'school_year'
                    },
                    {
                        data: 'date_start',
                        name: 'date_start'
                    },
                    {
                        data: 'date_end',
                        name: 'date_end'
                    },
                    {
                        data: 'grade_overall',
                        name: 'grade_overall'
                    },
                    {
                        data: 'grade_pass',
                        name: 'grade_pass'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        "searchable": false
                    }
                ]
            });
        });
    </script>
@endpush
