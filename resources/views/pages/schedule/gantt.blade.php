@push('css')
    <style>
        .gantt_cal_ltext {
            margin-bottom: 10px
        }

        .nested_task .gantt_add {
            display: none !important;
        }

        .gantt_grid_head_add {
            display: none !important;
        }
    </style>
@endpush
@push('js')
    <link href="{{ asset('assets/extra-libs/gantt/dhtmlxgantt.css?v=7.1.11') }}" rel="stylesheet">
    <script src="{{ asset('assets/extra-libs/gantt/dhtmlxgantt.js?v=7.1.11') }}"></script>
    <script type="text/javascript">
        function toggleMode(toggle) {
            gantt.$zoomToFit = !gantt.$zoomToFit;
            if (gantt.$zoomToFit) {
                toggle.innerHTML = "Set default Scale";
                //Saving previous scale state for future restore
                saveConfig();
                zoomToFit();
            } else {

                toggle.innerHTML = "Zoom to Fit";
                //Restore previous scale state
                restoreConfig();
                gantt.render();
            }
        }

        var cachedSettings = {};

        function saveConfig() {
            var config = gantt.config;
            cachedSettings = {};
            cachedSettings.scales = config.scales;
            cachedSettings.start_date = config.start_date;
            cachedSettings.end_date = config.end_date;
            cachedSettings.scroll_position = gantt.getScrollState();
        }

        function restoreConfig() {
            applyConfig(cachedSettings);
        }

        function applyConfig(config, dates) {

            gantt.config.scales = config.scales;

            // restore the previous scroll position
            if (config.scroll_position) {
                setTimeout(function() {
                    gantt.scrollTo(config.scroll_position.x, config.scroll_position.y)
                }, 4)
            }
        }

        function zoomToFit() {
            var project = gantt.getSubtaskDates(),
                areaWidth = gantt.$task.offsetWidth,
                scaleConfigs = zoomConfig.levels;

            for (var i = 0; i < scaleConfigs.length; i++) {
                var columnCount = getUnitsBetween(project.start_date, project.end_date, scaleConfigs[i].scales[scaleConfigs[
                    i].scales.length - 1].unit, scaleConfigs[i].scales[0].step);
                if ((columnCount + 2) * gantt.config.min_column_width <= areaWidth) {
                    break;
                }
            }


            if (i == scaleConfigs.length) {
                i--;
            }

            gantt.ext.zoom.setLevel(scaleConfigs[i].name);
            applyConfig(scaleConfigs[i], project);
        }

        // get number of columns in timeline
        function getUnitsBetween(from, to, unit, step) {
            var start = new Date(from),
                end = new Date(to);
            var units = 0;
            while (start.valueOf() < end.valueOf()) {
                units++;
                start = gantt.date.add(start, step, unit);
            }
            return units;
        }

        function zoom_in() {
            gantt.ext.zoom.zoomIn();
            gantt.$zoomToFit = false;
            document.querySelector(".zoom_toggle").innerHTML = "Zoom to Fit";
        }

        function zoom_out() {
            gantt.ext.zoom.zoomOut();
            gantt.$zoomToFit = false;
            document.querySelector(".zoom_toggle").innerHTML = "Zoom to Fit";
        }

        var zoomConfig = {
            levels: [
                // hours
                {
                    name: "hour",
                    scale_height: 27,
                    scales: [{
                            unit: "day",
                            step: 1,
                            format: "%d %M"
                        },
                        {
                            unit: "hour",
                            step: 1,
                            format: "%H:%i"
                        },
                    ]
                },
                // days
                {
                    name: "day",
                    scale_height: 27,
                    scales: [{
                        unit: "day",
                        step: 1,
                        format: "%d %M"
                    }]
                },
                // weeks
                {
                    name: "week",
                    scale_height: 50,
                    scales: [{
                            unit: "week",
                            step: 1,
                            format: function(date) {
                                var dateToStr = gantt.date.date_to_str("%d %M");
                                var endDate = gantt.date.add(date, -6, "day");
                                var weekNum = gantt.date.date_to_str("%W")(date);
                                return "#" + weekNum + ", " + dateToStr(date) + " - " + dateToStr(endDate);
                            }
                        },
                        {
                            unit: "day",
                            step: 1,
                            format: "%j %D"
                        }
                    ]
                },
                // months
                {
                    name: "month",
                    scale_height: 50,
                    scales: [{
                            unit: "month",
                            step: 1,
                            format: "%F, %Y"
                        },
                        {
                            unit: "week",
                            step: 1,
                            format: function(date) {
                                var dateToStr = gantt.date.date_to_str("%d %M");
                                var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
                                return dateToStr(date) + " - " + dateToStr(endDate);
                            }
                        }
                    ]
                },
                // quarters
                {
                    name: "quarter",
                    height: 50,
                    scales: [{
                            unit: "quarter",
                            step: 3,
                            format: function(date) {
                                var dateToStr = gantt.date.date_to_str("%M %y");
                                var endDate = gantt.date.add(gantt.date.add(date, 3, "month"), -1, "day");
                                return dateToStr(date) + " - " + dateToStr(endDate);
                            }
                        },
                        {
                            unit: "month",
                            step: 1,
                            format: "%M"
                        },
                    ]
                },
                // years
                {
                    name: "year",
                    scale_height: 50,
                    scales: [{
                        unit: "year",
                        step: 5,
                        format: function(date) {
                            var dateToStr = gantt.date.date_to_str("%Y");
                            var endDate = gantt.date.add(gantt.date.add(date, 5, "year"), -1, "day");
                            return dateToStr(date) + " - " + dateToStr(endDate);
                        }
                    }]
                },
                // decades
                {
                    name: "year",
                    scale_height: 50,
                    scales: [{
                            unit: "year",
                            step: 100,
                            format: function(date) {
                                var dateToStr = gantt.date.date_to_str("%Y");
                                var endDate = gantt.date.add(gantt.date.add(date, 100, "year"), -1, "day");
                                return dateToStr(date) + " - " + dateToStr(endDate);
                            }
                        },
                        {
                            unit: "year",
                            step: 10,
                            format: function(date) {
                                var dateToStr = gantt.date.date_to_str("%Y");
                                var endDate = gantt.date.add(gantt.date.add(date, 10, "year"), -1, "day");
                                return dateToStr(date) + " - " + dateToStr(endDate);
                            }
                        },
                    ]
                },
            ],
            element: function() {
                return gantt.$root.querySelector(".gantt_task");
            }
        };

        gantt.config.fit_tasks = true;
        gantt.ext.zoom.init(zoomConfig);
        gantt.ext.zoom.setLevel("month");
        gantt.$zoomToFit = false;

        gantt.config.grid_width = 380;
        gantt.config.add_column = false;
        gantt.config.columns = [{
                name: "text",
                label: "Name",
                tree: true,
                width: '*'
            },
            {
                name: "start_date",
                label: "Start time",
                align: "center"
            },
            {
                name: "duration",
                label: "Duration",
                align: "center"
            },
            {
                name: "add",
                label: "",
                width: 44
            }
        ];
        gantt.templates.grid_row_class = function(start, end, task) {
            if (task.$level > 0) {
                return "nested_task"
            }
            return "";
        };

        gantt.attachEvent("onTaskDblClick", function(id, e) {
            var task = gantt.getTask(id);
            console.log(task, 'TASK?')
            //any custom logic here
            return true;
        });

        gantt.locale.labels.section_rumah_sakit = "Rumah Sakit";
        gantt.locale.labels.section_stase = "Stase";
        gantt.locale.labels.section_nilai = "Nilai";

        let url = "/schedule/" + {{ $id }} +
            "/data?student_name={{ \Request::get('student_name') }}&hospital_id={{ \Request::get('hospital_id', 'all') }}&stase_id={{ \Request::get('stase_id', 'all') }}"

        gantt.load(url);

        function handleFilter() {
            $('#loading-el').toggleClass('d-none')
            const hospital_id = $('#hospital-select').val()
            const stase_id = $('#stase-select').val()
            const student_name = $('#student-search').val()

            url = "/schedule/" + {{ $id }} +
                `"/data?student_name=${student_name}&hospital_id=${hospital_id}&stase_id=${stase_id}`
            gantt.load(url)
                .then(function() {
                    $('#loading-el').toggleClass('d-none')
                });
            $('#filterModal').modal('hide')
        }

        var lightbox = [{
                name: "rumah_sakit",
                map_to: "hospital_id",
                height: 30,
                type: "select",
                options: getHospitals()
            },
            {
                name: "stase",
                map_to: "stase_id",
                height: 30,
                type: "select",
                options: getStases()
            },
            {
                name: "nilai",
                map_to: "score",
                height: 30,
                type: "textarea"
            },
            {
                name: "time",
                type: "duration",
                map_to: "auto",
                single_date: true
            }
        ];

        gantt.attachEvent("onBeforeLightbox", function(task_id) {
            gantt.resetLightbox();
            var task = gantt.getTask(task_id);
            gantt.config.lightbox.sections = lightbox;
            return true;
        });

        //WHEN CLICKED SAVE BUTTON ON MODAL
        gantt.attachEvent("onLightboxSave", function(id, item, is_new) {
            let success = false
            let start_date = getDateFormat(item.start_date);

            if (is_new) {
                const url = "{{ route('api.schedule-student.store') }}";
                $.ajax({
                    type: 'POST',
                    url: url,
                    async: false,
                    data: {
                        schedule_id: "{{ $id }}",
                        hospital_id: gantt.getLightboxSection('rumah_sakit').getValue(),
                        stase_id: gantt.getLightboxSection('stase').getValue(),
                        student_id: item.parent.replace('student-', ''), // student ID
                        date_start: start_date,
                        score: gantt.getLightboxSection('nilai').getValue()
                    },
                    success: function(data) {
                        gantt.load("/schedule/" + {{ $id }} + "/data");
                        success = true
                    },
                    error: function(request, error) {
                        success = false
                        alert(request.responseJSON.message)
                        gantt.message(request.responseJSON.message);
                    }
                })
            } else {
                let url = "{{ route('api.schedule-student.update', 'TASK_ID') }}";
                url = url.replace('TASK_ID', item.id);
                $.ajax({
                    type: 'PUT',
                    url: url,
                    async: false,
                    data: {
                        hospital_id: gantt.getLightboxSection('rumah_sakit').getValue(),
                        stase_id: gantt.getLightboxSection('stase').getValue(),
                        score: gantt.getLightboxSection('nilai').getValue(),
                        date_start: start_date,
                    },
                    success: function(data) {
                        gantt.load("/schedule/" + {{ $id }} + "/data");
                        success = true
                    },
                    error: function(request, error) {
                        success = false
                        alert(request.responseJSON.message)
                        gantt.message(request.responseJSON.message);
                    }
                })
            }

            return success

        });

        gantt.attachEvent("onLightboxDelete", function(id) {
            let url = "{{ route('api.schedule-student.destroy', 'TASK_ID') }}";
            url = url.replace('TASK_ID', id);
            let success = false
            $.ajax({
                type: 'DELETE',
                url: url,
                async: false,
                success: function(data) {
                    gantt.load("/schedule/" + {{ $id }} + "/data");
                    success = true
                },
                error: function(request, error) {
                    success = false
                    alert('Terjadi kesalahan')
                }
            })

            return success

        });

        function f(task) {
            let task_id = task.id;
            let start_date = getDateFormat(task.start_date);
            let end_date = getDateFormat(task.end_date);

            let url = "{{ route('schedule.drag', 'TASK_ID') }}";
            url = url.replace('TASK_ID', task_id);

            return $.ajax({
                type: 'POST',
                url: url,
                data: {
                    date_start: start_date,
                    date_end: end_date
                },
                success: function(data) {
                    return true;
                },
                error: function(request, error) {
                    alert(request.responseJSON.message)
                    gantt.message(request.responseJSON.message);
                    return false;
                },
            });
        };

        gantt.attachEvent("onBeforeTaskChanged", async function(id, mode, old_task) {
            var task = gantt.getTask(id);
            return await f(task);
        });

        gantt.init("gantt_here");

        function getDateFormat(date) {
            return new Date(date).getFullYear() + '-' + (new Date(date).getMonth() + 1) + '-' + new Date(date).getDate();
        }

        function getHospitals() {
            let result = []
            $.ajax({
                url: '{{ route('api.hospital.index') }}',
                async: false,
                success: function(data) {
                    result = data.data.map(i => {
                        return {
                            key: i.id,
                            label: i.name
                        }
                    })
                }
            })
            return result
        }

        function getStases() {
            let result = []
            $.ajax({
                url: '{{ route('api.stase.index') }}',
                async: false,
                success: function(data) {
                    result = data.data.map(i => {
                        return {
                            key: i.id,
                            label: i.name
                        }
                    })
                }
            })
            return result
        }
    </script>
@endpush
