@extends('layouts.app')
@section('content')
    <form action="{{ route($module . '.store') }}" method="post">
        @csrf
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <h4 class="card-title">Jadwal</h4>
                                    <h6 class="card-subtitle">Pilih Mahasiswa</h6>
                                </div>
                                <div class="col-md-4" align="right">
                                    <a href="{{ route($module . '.create') }}" class="btn btn-danger btn-lg"><i
                                            class="m-r-10 mdi mdi-backspace"></i>Kembali</a>
                                    <button class="btn btn-success btn-lg" type="submit">Submit<i
                                            class="m-l-10 mdi mdi-arrow-right-bold"></i></button>
                                </div>
                                @foreach ($schedule_datas as $k => $schedule_data)
                                    <input type="hidden" name="{{ $k }}" value="{{ $schedule_data }}" />
                                @endforeach
                            </div>
                            <hr>
                            <div class="row">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Angkatan</th>
                                            <th>Nama Mahasiswa</th>
                                            <th>Aksi</th>
                                        </tr>

                                    </thead>
                                    <tbody>
                                        @foreach ($students as $student)
                                            <tr>
                                                <td>{{ $student->grade->name }}</td>
                                                <td>{{ $student->name }}</td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                            value="{{ $student->id }}" name="student_id[]"
                                                            id="flexCheckChecked">
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                </table>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@stop
