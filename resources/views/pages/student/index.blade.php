@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="col-md-6">
                            <h4 class="card-title">Mahasiswa</h4>
                            <h6 class="card-subtitle">Pengelolaan Mahasiswa</h6>
                        </div>
                        <div class="d-flex">
                            <div class="mr-2" align="right">
                                <a href="{{ route('students.export') }}" class="btn btn-primary btn-lg"><i
                                        class="fa fa-download"></i> Export</a>
                            </div>
                            @can('student.create')
                            <div class="mr-2" align="right">
                                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                    data-target="#exampleModal"><i class="fa fa-upload"></i> Import</button>
                            </div>
                            @endcan
                            @can('student.create')
                            <div class="" align="right">
                                <a href="{{ route($module . '.create') }}" class="btn btn-success btn-lg"><i
                                        class="fa fa-plus"></i> Tambah</a>
                            </div>
                            @endcan
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Status</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form action="{{ route('students.import') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Data Mahasiswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <a href="{{ asset('assets/template/mahasiswa.xlsx') }}" download class="btn btn-primary btn-lg"><i
                            class="fa fa-download"></i> Download Template</a>
                    <div class="border p-3 mt-3">
                        <div for="import" class="mb-2">Upload Excel</div>
                        <input type="file" name="file">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>
@stop
@push('js')
<script>
    $(document).ready(function() {
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route($module . '.index') }}',
                columns: [{
                        data: 'nim',
                        name: 'nim'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'gender',
                        name: 'gender'
                    },
                    {
                        data: 'birth_date',
                        name: 'birth_date'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },{
                        data: 'email',
                        name: 'email'
                    },{
                        data: 'phone',
                        name: 'phone'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        "searchable": false
                    }
                ]
            });
        });
</script>
@endpush
