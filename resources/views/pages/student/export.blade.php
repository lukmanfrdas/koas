<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Nim</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Nomor Hp</th>
        <th>Jenis Kelamin</th>
        <th>Tanggal Lahir</th>
        <th>Status</th>
        <th>Angkatan</th>
    </tr>
    </thead>
    <tbody>
    @foreach($students as $student)
        <tr>
            <th>{{ $loop->iteration }}</th>
            <td>{{ $student->nim ?? '-' }}</td>
            <td>{{ $student->name ??'-' }}</td>
            <td>{{ $student->email ?? '-' }}</td>
            <td>{{ $student->phone ?? '-' }}</td>
            <td>{{ $student->gender ?? '-' }}</td>
            <td>{{ $student->birth_date ?? '-' }}</td>
            <td>{{ $student->status ?? '-'}}</td>
            <td>{{ $student->grade->name ?? '' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
