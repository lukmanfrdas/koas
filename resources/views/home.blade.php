@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class='input-group mb-3'>
                    <div id="reportrange"
                        style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </div> -->
        </div>

        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="m-r-10">
                            <span class="btn btn-circle btn-lg bg-danger">
                                <i class="ti-user text-white"></i>
                            </span>
                        </div>
                        <div>
                            Jumlah Mahasiswa
                        </div>
                        <div class="ml-auto">
                            <h2 class="m-b-0 font-light">{{ $students }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="m-r-10">
                            <span class="btn btn-circle btn-lg btn-info">
                                <i class="ti-list-ol text-white"></i>
                            </span>
                        </div>
                        <div>
                            Jumlah Angkatan
                        </div>
                        <div class="ml-auto">
                            <h2 class="m-b-0 font-light">{{ $grades }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="m-r-10">
                            <span class="btn btn-circle btn-lg bg-warning">
                                <i class="mdi mdi-account-convert text-white"></i>
                            </span>
                        </div>
                        <div>
                            Mahasiswa Berjalan
                        </div>
                        <div class="ml-auto">
                            <h2 class="m-b-0 font-light">{{ $running_students }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="m-r-10">
                            <span class="btn btn-circle btn-lg btn-info">
                                <i class="ti-list-ol text-white"></i>
                            </span>
                        </div>
                        <div>
                            Jumlah Mahasiswa Yang Belum
                        </div>
                        <div class="ml-auto">
                            <h2 class="m-b-0 font-light">{{ $students_yet }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="m-r-10">
                            <span class="btn btn-circle btn-lg bg-danger">
                                <i class="ti-user text-white"></i>
                            </span>
                        </div>
                        <div>
                            Jumlah Mahasiswa Yang Sudah
                        </div>
                        <div class="ml-auto">
                            <h2 class="m-b-0 font-light">{{ $students_already }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <h5 class="card-title m-b-0 align-self-center chart-type">
                    <a data-value="students_grade" class="btn btn-primary btn-report" href="#" role="button">Mahasiswa per angkatan</a>
                    <a data-value="kuota_hospitals" class="btn btn-primary btn-report" href="#" role="button">Kouta rumah sakit</a>
                </h5>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="card border-top-0">
                    <div id="bar-chart"></div>
                </div>
            </div>
            {{-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="card border-top-0">
                    <div id="kuota-chart"></div>
                </div>
            </div> --}}
        </div>
    </div>
@endsection

@push('css')
    <link href="{{ asset('assets/libs/morris.js/morris.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
    <style>
        #bar-chart{
            min-height: 250px;
        }
    </style>
@endpush
@push('js')
    <script src="{{ asset('assets/libs/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.8.0/chart.min.js"></script>
    <script>

$(function() {
            var start = moment().subtract(1, 'months');
            var end = moment();

            $(".btn-report").click(function() {
                if ($(this).hasClass('btn-primary')) {
                    $(".btn-report").removeClass('btn-secondary').addClass('btn-primary');
                    $(this).removeClass('btn-primary').addClass('btn-secondary');
                    setBar();
                }
            });
            $(".chart-type a:first").trigger('click');
        });

        function setBar() {
            $("#bar-chart").empty();

            var active = $(".btn-report.btn-secondary").data('value');
            console.log(active)
            var chart = Morris.Bar({
                element: 'bar-chart',
                ykeys: 'a',
                xkey: 'y',
                labels: [active],
                fillOpacity: 0.6,
                hideHover: 'auto',
                behaveLikeLine: true,
                resize: true,
                pointFillColors:['#ffffff'],
                pointStrokeColors: ['black'],
                lineColors:['gray','red'],
            });

            $.getJSON("{{ route('home.report') }}", {
                    type: active,
                },
                function(data, textStatus, jqXHR) {
                    chart.setData(data);
                    $("#color-info").html('<ul class="list-inline text-right">' +
                        '<li class="list-inline-item">' +
                        '<h5><i class="fa fa-circle m-r-5 text-primary"></i>' + active + '</h5>' +
                        '</li>' +
                        '</ul>');
                }
            );
        }
    </script>
@endpush
