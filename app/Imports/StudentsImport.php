<?php

namespace App\Imports;

use App\Models\Grade;
use App\Models\Student;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;

class StudentsImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        try {
            $now = date('Y-m-d H:i:s');
            foreach ($rows as $key => $row) {
                if ($key > 0 && $row[5]) {
                    $grade = Grade::firstOrCreate(['name' => $row[5]]);
                    //  Grade::findOrCreate(['name' => $row[5]], ['name' => $row[5]]);
                    $student = [
                        'nim' => $row[0],
                        'name' => $row[1],
                        'gender' => $row[2],

                        // TODO:: tambahkan keterangan error apabila format birth_date tidak sesuai

                        // 'birth_date' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[3])),
                        'birth_date' => '2000-01-01',
                        'status' => strtolower($row[4]),
                        'email' => $row[6],
                        'phone' => $row[7],
                        'created_at' => $now,
                        'grade_id' => $grade->id
                    ];
                    // Student::updateOrInsert(['nim' => $row[0]], $student);
                    Student::updateOrInsert(['email' => $row[6]], $student);
                }
            }
        } catch (\Exception $ex) {
            dd($ex);
        }
    }
}
