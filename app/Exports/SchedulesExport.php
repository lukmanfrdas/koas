<?php

namespace App\Exports;

use App\Models\Schedule;
use App\Models\Student;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;

class SchedulesExport  implements FromView
{
    protected $id;

    function __construct($id) {
        $this->id = $id;
        // dd($this->id);
        dd('Modul Sedang Tahap Pengembangan, Mohon maaf atas ketidaknyamanannya');
    }


    public function view(): View
    {
        return view('pages.student.export', [
            'students' => Student::all()
        ]);
    }
}
