<?php

namespace App\Http\Controllers;

use App\Models\Grade;
use App\Models\Hospital;
use App\Models\ScheduleStudent;
use App\Models\Student;
use App\Models\Quota;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [];
        $todayDate = Carbon::now()->format('Y-m-d');
        $data['students'] = Student::count();
        $data['grades'] = Grade::count();
        $data['running_students'] = Student::has('schedule_students')->count();
        $data['students_already'] = Student::whereHas('schedule_students', function($q)use($todayDate){
            $q->where('date_end', '>', $todayDate);
        })->count();
        $data['students_yet'] = Student::whereHas('schedule_students', function($q)use($todayDate){
            $q->where('date_end', '<=', $todayDate);
        })->count();

        return view('home', $data);
    }

    public function report(Request $request)
    {
        $data = [];

        if($request->type == 'students_grade') {
            $grades = Grade::get(['id', 'name']);
            $temp = [];
            foreach($grades as $key => $grade) {
                $temp[$key] = [
                    'y' => $grade->name
                ];
            }

            $students = Student::query()
                    ->selectRaw('count(*) as total, grade_id')
                    ->groupBy('grade_id')
                    ->get();

            foreach($students as $key => $student) {
                $temp[$key]['a'] = $student->total;
            }

            $data = $temp;
        } else {
            $hospitals = Hospital::with(['quotas', 'schedule_students'])->get();
            foreach ($hospitals as $hospital) {
                $quota = $hospital->quotas->sum('quota');
                $count_student_hospital = $hospital->schedule_students->groupBy('student_id')->count();
                if (!empty($quota)) {
                    $remaining_quota = $quota - $count_student_hospital;
                    $data[] = [
                        'y' => $hospital->name,
                        'a' => number_format($remaining_quota)
                    ];
                }
            }
        }

        return response()->json($data, 200);
    }
}
