<?php

namespace App\Http\Controllers;

use App\Exports\SchedulesExport;
use App\Traits\XIForm;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Http\Requests\ScheduleRequest;
use App\Models\Grade;
use App\Models\Hospital;
use App\Models\HospitalStase;
use App\Models\Level;
use App\Models\Quota;
use App\Models\ScheduleStudentGroup;
use App\Models\ScheduleStudentGroupDetail;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\Schedule;
use App\Models\ScheduleStudent;
use App\Models\Stase;
use App\Models\Student;
use ArrayObject;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

class ScheduleController extends Controller
{
    private $module, $model, $form;
    protected $repository;
    use XIForm;

    public function __construct(Schedule $repository, FormBuilder $formBuilder)
    {
        $this->module = 'schedule';
        $this->repository = $repository;
        $this->formBuilder = $formBuilder;
        $this->form = 'App\Forms\ScheduleForm';
        $this->formRequest = 'App\Http\Requests\ScheduleRequest';

        View::share('module', $this->module);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        if ($request->ajax()) {
            $data = $this->repository->orderBy('created_at', 'ASC');
            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    $buttons[] = ['type' => 'report', 'route' => route($this->module . '.report', $data->id), 'label' => 'Report', 'action' => 'primary', 'icon' => 'book'];
                    $buttons[] = ['type' => 'detail', 'route' => route($this->module . '.show', $data->id), 'label' => 'Detail', 'action' => 'primary', 'icon' => 'share'];
                    $buttons[] = ['type' => 'edit', 'route' => route($this->module . '.edit', $data->id), 'label' => 'Edit', 'icon' => 'edit'];
                    $buttons[] = ['type' => 'delete', 'label' => 'Delete', 'confirm' => 'Are you sure?', 'route' => route($this->module . '.destroy', $data->id)];

                    return $this->icon_button($buttons, false);
                })
                ->make();
        }
        return view('pages.' . $this->module . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'POST',
            'url' => route($this->module . '.store')
        ]);
        $data['validator']  = JsValidatorFacade::formRequest($this->formRequest);
        $data['grades']     = Grade::get();
        $data['levels']     = Level::get();
        return view('pages.' . $this->module . '.create', $data);
    }

    public function select_student(Request $request)
    {
        $input                  = $request->all();

        $data['schedule_datas']  = [
            'code'          => $input['code'],
            'name'          => $input['name'],
            'description'   => $input['description'],
            'date_start'    => $input['date_start'],
            'date_end'      => $input['date_end'],
            'grade_id'      => $input['grade_id'],
            'level_id'      => $input['level_id'],
            'group_content' => $input['group_content'],
        ];
        // dd($input);
        $data['students']   = Student::whereGradeId($input['grade_id'])->get();

        return view('pages.' . $this->module . '.select_student', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScheduleRequest $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        DB::beginTransaction();
        try {
            $data = $request->all();


            // Create schedule data
            $scheduleData = [
                'code'          => $data['code'],
                'name'          => $data['name'],
                'description'   => $data['description'],
                'level_id'   => $data['level_id'],
                'date_start'    => $data['date_start'],
                'date_end'      => $data['date_end'],
            ];
            $schedule = $this->repository->create($scheduleData);


            /*
                Generating a schedule based on conditions:
                    1. Organize groups in accordance with the number of groups (student_id)
                    2. The station of each student is random
                    3. It is especially important for the IKM and MATRA stations that have been scheduled in the last six months
            */
            $student_groups = array_chunk($data['student_id'], $data['group_content']);
            $stases = Stase::whereLevelId($data['level_id'])->get();
            $hospitals = Hospital::orderBy('sequence', 'asc')->get();
            $no = 1;
            foreach ($student_groups as $group => $student_group) {
                $stases = $stases->shuffle();
                $schedule_student_group = ScheduleStudentGroup::create([
                    'schedule_id' => $schedule->id,
                    'name' => $group,
                ]);


                for ($i = 0; $i < count($student_group); $i++) {
                    ScheduleStudentGroupDetail::create([
                        'schedule_student_group_id' => $schedule_student_group->id,
                        'student_id' => $student_group[$i]
                    ]);


                    $date_end = null;
                    $schedule_students = collect();

                    foreach ($stases as $stase) {

                        if ($date_end) {
                            $date_start = $date_end;
                            $date_end_raw = date('Y-m-d', strtotime("+" . $stase->week . " week", strtotime($date_end)));
                        } else {
                            $date_start = $data['date_start'];
                            $date_end_raw = date('Y-m-d', strtotime("+" . $stase->week . " week", strtotime($data['date_start'])));
                        }



                        /*
                            Validation
                            - It is especially important for the IKM and MATRA stations that have been scheduled in the last six months
                        /*/
                        $origin = new DateTime($date_start);
                        $target = new DateTime($data['date_end']);
                        $interval = $origin->diff($target);
                        if ($interval->format('%m') > 6) {
                            if (in_array($stase->name, ['IKM', 'MATRA'])) continue;
                        } else {
                            $stase_data = $stases->whereNotIn('id', $schedule_students->pluck('stase_id'))->whereIn('name', ['IKM', 'MATRA'])->first();
                            if ($stase_data) $stase = $stase_data;
                        }



                        /*
                            Validation
                            - Check quotas based on hospital and station quotas
                        /*/
                        $selected_quota = null;
                        foreach ($hospitals as $hospital) {
                            $quota = Quota::with('hospital')->whereHospitalId($hospital->id)->whereStaseId($stase->id)->first();
                            $count_student_hospital = ScheduleStudent::whereHospitalId($hospital->id)->whereStaseId($stase->id)->whereScheduleId($schedule->id)->count();
                            if (!empty($quota)) {
                                $remaining_quota = $quota->quota - $count_student_hospital;
                                if ($remaining_quota > 0) {
                                    $selected_quota = $hospital;
                                    break;
                                }
                            }
                        }



                        /*
                            Validation
                            - The data is included in the schedule if there is a quota
                        /*/
                        if (!empty($selected_quota)) {
                            $date_end = $date_end_raw;

                            $schedule_student = [
                                'schedule_id'   => $schedule->id,
                                'student_id'    => $student_group[$i],
                                'stase_id'      => $stase->id,
                                'hospital_id'   => $selected_quota->id,
                                'date_start'    => $date_start,
                                'date_end'      => $date_end,
                                'created_at'    => date('Y-m-d H:i:s'),
                                'updated_at'    => date('Y-m-d H:i:s'),
                            ];
                            $schedule_students->add($schedule_student);
                        }
                    }


                    ScheduleStudent::insert($schedule_students->toArray());
                    $no++;
                }
            }

            DB::commit();
            // gilog("Create " . $this->module, $student_schedules, $data);
            flash('Success create ' . $this->module)->success();
            return redirect()->route($this->module . '.index');
        } catch (\Exception $ex) {
            DB::rollBack();
            flash($ex->getMessage())->error();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $get = $this->repository->with('schedule_student_groups')->whereId($id)->first();
        $data['id'] = $id;
        $data['detail'] = $get;
        $detail = new Schedule();
        $data['shows'] = $detail->getFillable();

        $data['total_week'] = Stase::sum('week');
        $data['stases']     = Stase::get();
        $data['hospitals']  = Hospital::get();
        $data['dashboard'] = [];
        foreach ($data['hospitals'] as $hospital) {
            foreach ($data['stases'] as $stase) {
                $data['dashboard'][$hospital->id][$stase->id]['student']    = ScheduleStudent::whereScheduleId($get->id)
                    ->whereHospitalId($hospital->id)
                    ->whereStaseId($stase->id)
                    ->count();
                $quota = Quota::whereHospitalId($hospital->id)->whereStaseId($stase->id)->first();
                $data['dashboard'][$hospital->id][$stase->id]['quota']      = @$quota->quota;
            }
        }

        return view('pages.' . $this->module . '.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        $get = $this->repository->find($id);
        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'PUT',
            'url' => route($this->module . '.update', $id),
            'model' => $get
        ]);

        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ScheduleRequest $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        try {
            DB::transaction(function () use ($id, $request) {
                $input = $request->all();
                $post = $this->repository->find($id);
                $post->update($input);
                gilog("Create " . $this->module, $post, $input);
            });
            flash('Success update ' . $this->module)->success();
        } catch (\Exception $ex) {
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.delete')) return notPermited('json');

        try {
            DB::transaction(function () use ($id) {
                $get = $this->repository->find($id);
                $get->delete($id);
                gilog("Delete " . $this->module, $get, ['notes' => @request('notes')]);
            });
            $data['message'] = 'Success delete ' . $this->module;
            $status = 200;
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    public function update_schedule_student(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        try {
            DB::transaction(function () use ($request) {
                $input = $request->all();
                $post = ScheduleStudent::find($input['id']);
                $post->update($input);
                gilog("Create " . $this->module, $post, $input);
            });
            flash('Success update ' . $this->module)->success();
        } catch (\Exception $ex) {
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.show', $id);
    }

    public function data(Request $request, $id)
    {
        $get = $this->repository->with('schedule_student_groups')->whereId($id)->first();

        $no = 0;
        $data = [];
        foreach ($get->schedule_student_groups as $schedule_student_group) {
            foreach ($schedule_student_group->schedule_student_group_details as $group_detail) {
                $schedule_students = ScheduleStudent::whereScheduleId($schedule_student_group->schedule_id)
                    ->whereStudentId($group_detail->student_id)
                    ->orderBy('date_start', 'asc');

                if ($request->hospital_id !== 'all') {
                    $schedule_students = $schedule_students->where('hospital_id', $request->hospital_id);
                }

                if ($request->stase_id !== 'all') {
                    $schedule_students = $schedule_students->where('stase_id', $request->stase_id);
                }

                if (!empty($request->student_name)) {
                    $schedule_students = $schedule_students->whereHas('student', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request->student_name . '%');
                    });
                }

                $schedule_students = $schedule_students->get();

                if ($schedule_students->count() == 0) continue;


                $parent_id = "student-" . $group_detail->student_id;

                $parent_date_start = clone collect($schedule_students);
                $parent_date_start = $parent_date_start->sortBy('date_start')->first();
                $parent_date_start = data_get($parent_date_start, 'date_start');

                $parent_date_end = clone collect($schedule_students);
                $parent_date_end = $parent_date_end->sortByDesc('date_end')->first();
                $parent_date_end = data_get($parent_date_end, 'date_end');

                $date1 = date_create($parent_date_start);
                $date2 = date_create($parent_date_end);
                $diff = date_diff($date1, $date2);
                $duration = $diff->format('%a');

                $data['tasks'][$no]['id'] = $parent_id;
                $data['tasks'][$no]['text'] = $group_detail->student->name ?? '';
                $data['tasks'][$no]['start_date'] = date("d-m-Y", strtotime($parent_date_start));
                $data['tasks'][$no]['duration'] = $duration;
                $data['tasks'][$no]['progress'] = 0;
                $data['tasks'][$no]['open'] = true;
                $data['tasks'][$no]['holder'] = $schedule_student_group->name;
                $data['tasks'][$no]['hospital_id'] = null;
                $data['tasks'][$no]['stase_id'] = null;
                $data['tasks'][$no]['parent'] = null;
                $data['tasks'][$no]['color'] = null;
                $data['tasks'][$no]['score'] = null;
                $data['tasks'][$no]['close'] = true;
                $data['tasks'][$no]['open'] = false;

                $no++;
                foreach ($schedule_students as $k => $schedule_student) {
                    // if(!empty($shedule_student->hospital)) {
                    $date1 = date_create($schedule_student->date_start);
                    $date2 = date_create($schedule_student->date_end);
                    $diff = date_diff($date1, $date2);
                    $duration = $diff->format('%a');

                    $data['tasks'][$no]['id'] = $schedule_student->id;
                    $data['tasks'][$no]['text'] = $schedule_student->stase->name . "(" . $schedule_student->hospital->name . ")";
                    $data['tasks'][$no]['start_date'] = date("d-m-Y", strtotime($schedule_student->date_start));
                    $data['tasks'][$no]['duration'] = $duration;
                    $data['tasks'][$no]['progress'] = 0;
                    $data['tasks'][$no]['open'] = true;
                    $data['tasks'][$no]['holder'] = $schedule_student->stase->name;
                    $data['tasks'][$no]['hospital_id'] = $schedule_student->hospital->id;
                    $data['tasks'][$no]['stase_id'] = $schedule_student->stase->id;
                    $data['tasks'][$no]['parent'] = $parent_id;
                    $data['tasks'][$no]['color'] = $schedule_student->hospital->color;
                    $data['tasks'][$no]['score'] = $schedule_student->score;
                    $data['tasks'][$no]['open'] = false;
                    // }

                    $no++;
                }
            }
        }

        return response()->json($data);
    }

    public function drag(Request $request, $id)
    {
        $input = $request->all();
        $post = ScheduleStudent::find($id);

        $check_time = ScheduleStudent::query()
            ->whereScheduleId($post->schedule_id)
            ->whereStudentId($post->student_id)
            ->where(function ($query) use ($input) {
                $query
                    ->orWhereBetween('date_start', [date('Y-m-d', strtotime("+1 day", strtotime($input['date_start']))), $input['date_end']])
                    ->orWhereBetween('date_end', [date('Y-m-d', strtotime("+1 day", strtotime($input['date_start']))), $input['date_end']]);
            })
            ->where('id', '!=', $id)
            ->exists();

        if ($check_time) {
            $data['message'] = 'Duplicate Time';
            $status = 500;
            return response()->json($data, $status);
        }

        $post->update($input);

        return response()->json($post);
    }

    public function report(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $get = $this->repository->find($id);
        $data['id'] = $id;
        $data['detail'] = $get;

        $sql_get_grade_overall_count = "(select count(id) from schedule_students where schedule_id='$id' AND student_id=ss.student_id)";
        $sql_get_grade_overall = "(select count(score)/$sql_get_grade_overall_count from schedule_students where schedule_id='$id' AND student_id=ss.student_id) as grade_overall";

        $sql_get_grade_pass_count = "(select count(stase_id) from schedule_students where schedule_id='$id' AND student_id=ss.student_id)";
        $sql_get_grade_pass_in = "(select stase_id from schedule_students where schedule_id='$id' AND student_id=ss.student_id)";
        $sql_get_grade_pass = "(select sum(score)/$sql_get_grade_pass_count from stases where id in $sql_get_grade_pass_in) as grade_pass";

        if ($request->ajax()) {
            $data = ScheduleStudent::query()
                ->from('schedule_students as ss')
                ->selectRaw("*, $sql_get_grade_overall, $sql_get_grade_pass")
                ->with('student')
                ->whereScheduleId($id)
                ->groupBy('student_id');

            return DataTables::of($data)
                ->addColumn('action', function ($data) use ($id) {
                    $buttons = [];
                    $buttons[] = ['type' => 'detail', 'route' => route($this->module . '.report-detail', ['schedule_id' => $id, 'student_id' => $data->student_id]), 'label' => 'Detail', 'action' => 'primary', 'icon' => 'share'];

                    return $this->icon_button($buttons, true);
                })
                ->addColumn('group', function ($data) {
                    $schedule_student_group = ScheduleStudentGroup::where('schedule_id', $data->schedule_id)->first();
                    return data_get($schedule_student_group, 'name', '-');
                })
                ->addColumn('school_year', function ($data) {
                    return date('Y', strtotime($data->date_start));
                })
                ->make();
        }

        return view('pages.' . $this->module . '.report', $data);
    }

    public function reportDetail(Request $request, $schedule_id, $student_id)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $data['schedule_id'] = $schedule_id;
        $data['student_id'] = $student_id;

        if ($request->ajax()) {
            $data = ScheduleStudent::query()
                ->from('schedule_students as ss')
                ->with(['student', 'stase', 'hospital'])
                ->whereScheduleId($schedule_id)
                ->whereStudentId($student_id);

            return DataTables::of($data)
                ->addColumn('group', function ($data) {
                    $schedule_student_group = ScheduleStudentGroup::where('schedule_id', $data->schedule_id)->first();
                    return data_get($schedule_student_group, 'name', '-');
                })
                ->addColumn('school_year', function ($data) {
                    return date('Y', strtotime($data->date_start));
                })
                ->addColumn('status', function ($data) {
                    if ($data->score > data_get($data, 'stase.score', 0)) {
                        return '<label class="label label-success">LULUS</label>';
                    } else {
                        return '<label class="label label-danger">TIDAK LULUS</label>';
                    }
                })
                ->rawColumns(['status'])
                ->make();
        }

        return view('pages.' . $this->module . '.report-detail', $data);
    }

    public function fullscreen(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $get = $this->repository->with('schedule_student_groups')->whereId($id)->first();
        $data['id'] = $id;
        $data['detail'] = $get;
        $detail = new Schedule();
        $data['shows'] = $detail->getFillable();

        $data['total_week'] = Stase::sum('week');
        $data['stases']     = Stase::get();
        $data['hospitals']  = Hospital::get();
        // $data['dashboard'] = [];
        // foreach ($data['hospitals'] as $hospital) {
        //     foreach ($data['stases'] as $stase) {
        //         $data['dashboard'][$hospital->id][$stase->id]['student']    = ScheduleStudent::whereScheduleId($get->id)
        //             ->whereHospitalId($hospital->id)
        //             ->whereStaseId($stase->id)
        //             ->count();
        //         $quota = Quota::whereHospitalId($hospital->id)->whereStaseId($stase->id)->first();
        //         $data['dashboard'][$hospital->id][$stase->id]['quota']      = @$quota->quota;
        //     }
        // }

        $form = $this->formBuilder->create('App\Forms\StudentAddForm', [
            'method' => 'PUT',
            'model' => ['schedule_id' => $id]
        ]);
        $buttons[] = [
            'label' => 'Tambah Siswa',
            'route' => route('api.schedule-student.store-new-student', ['schedule_id' => $id]),
            'confirm' => 'Are you sure?',
            'form' => $form,
            'class' => 'btn-lg btn-primary',
            'action' => 'primary',
            'validation' => "App\Http\Requests\GradeRequest"
        ];
        $data['buttons'] = $this->icon_button($buttons);
        return view('pages.' . $this->module . '.fullscreen', $data);
    }

    public function export($id)
    {
        return Excel::download(new SchedulesExport($id), 'students in schedules.xlsx');
    }
}
