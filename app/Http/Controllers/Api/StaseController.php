<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Stase;
use Illuminate\Http\Request;

class StaseController extends Controller
{
    public function index() {
        try {
            $stases = Stase::all();
            return response()->json($stases);
        } catch (\Exception $ex) {
            return response(400)->json($ex->getMessage());
        }
    }
}
