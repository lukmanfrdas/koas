<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Quota;
use App\Models\ScheduleStudent;
use App\Models\ScheduleStudentGroup;
use App\Models\ScheduleStudentGroupDetail;
use App\Models\Stase;
use Illuminate\Http\Request;

class ScheduleStudentController extends Controller
{
    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $post = ScheduleStudent::find($id);
            $stase = Stase::find($request->stase_id);
            $input['date_end'] = date('Y-m-d', strtotime("+" . $stase->week . " week", strtotime(data_get($input, 'date_start'))));

            $check_time = ScheduleStudent::query()
                ->whereScheduleId($post->schedule_id)
                ->whereStaseId($request->stase_id)
                ->whereStudentId($post->student_id)
                ->where(function ($query) use ($input) {
                    $query
                        ->orWhereBetween('date_start', [date('Y-m-d', strtotime("+1 day", strtotime($input['date_start']))), $input['date_end']])
                        ->orWhereBetween('date_end', [date('Y-m-d', strtotime("+1 day", strtotime($input['date_start']))), $input['date_end']]);
                })
                ->where('id', '!=', $id)
                ->exists();
            if ($check_time) {
                $data['message'] = 'Duplicate Time';
                $status = 500;
                return response()->json($data, $status);
            }

            $check_clash = ScheduleStudent::query()
                ->whereScheduleId($post->schedule_id)
                ->whereStaseId($request->stase_id)
                ->whereStudentId($post->student_id)
                ->whereHospitalId($request->hospital_id)
                ->where('id', '!=', $id)
                ->exists();
            if ($check_clash) {
                $data['message'] = 'Duplicate Bentrok';
                $status = 500;
                return response()->json($data, $status);
            }

            $quota = Quota::with('hospital')
                ->whereHospitalId($request->hospital_id)
                ->whereStaseId($stase->id)
                ->first();
            $count_student_hospital = ScheduleStudent::query()
                ->whereScheduleId($post->schedule_id)
                ->whereHospitalId($request->hospital_id)
                ->whereStaseId($stase->id)
                ->where('id', '!=', $id)
                ->count();

            $check_quota = true;
            if (!empty($quota)) {
                $remaining_quota = $quota->quota - $count_student_hospital;
                if ($remaining_quota > 0) {
                    $check_quota = false;
                }
            }
            if ($check_quota) {
                $data['message'] = 'Quota';
                $status = 500;
                return response()->json($data, $status);
            }

            $post->update($input);
            $data['message'] = 'Sukses update data';
            $status = 200;
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $stase = Stase::find($request->stase_id);
            $input['date_end'] = date('Y-m-d', strtotime("+" . $stase->week . " week", strtotime(data_get($input, 'date_start'))));

            $check_time = ScheduleStudent::query()
                ->whereScheduleId($request->schedule_id)
                ->whereStaseId($request->stase_id)
                ->whereStudentId($request->student_id)
                ->where(function ($query) use ($input) {
                    $query
                        ->orWhereBetween('date_start', [date('Y-m-d', strtotime("+1 day", strtotime($input['date_start']))), $input['date_end']])
                        ->orWhereBetween('date_end', [date('Y-m-d', strtotime("+1 day", strtotime($input['date_start']))), $input['date_end']]);
                })
                ->exists();
            if ($check_time) {
                $data['message'] = 'Duplicate Time';
                $status = 500;
                return response()->json($data, $status);
            }

            $check_clash = ScheduleStudent::query()
                ->whereScheduleId($request->schedule_id)
                ->whereStaseId($request->stase_id)
                ->whereStudentId($request->student_id)
                ->whereHospitalId($request->hospital_id)
                ->exists();
            if ($check_clash) {
                $data['message'] = 'Duplicate Bentrok';
                $status = 500;
                return response()->json($data, $status);
            }

            $quota = Quota::with('hospital')
                ->whereHospitalId($request->hospital_id)
                ->whereStaseId($stase->id)
                ->first();
            $count_student_hospital = ScheduleStudent::query()
                ->whereScheduleId($request->schedule_id)
                ->whereHospitalId($request->hospital_id)
                ->whereStaseId($stase->id)
                ->count();

            $check_quota = true;
            if (!empty($quota)) {
                $remaining_quota = $quota->quota - $count_student_hospital;
                if ($remaining_quota > 0) {
                    $check_quota = false;
                }
            }
            if ($check_quota) {
                $data['message'] = 'Quota';
                $status = 500;
                return response()->json($data, $status);
            }

            $store = ScheduleStudent::create($input);
            if ($store) {
                $data['message'] = 'Sukses tambah data';
                $status = 200;
            } else {
                $data['message'] = 'Data gagal disimpan';
                $status = 500;
            }
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    public function destroy(Request $request, $id)
    {
        try {
            $get = ScheduleStudent::find($id);
            $get->delete($id);
            $data['message'] = 'Success delete data';
            $status = 200;
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    public function storeNewStudent(Request $request)
    {
        try {
            $input = $request->all();

            $stase = Stase::find($request->stase_id);
            $input['date_end'] = date('Y-m-d', strtotime("+" . $stase->week . " week", strtotime(data_get($input, 'date_start'))));

            $check_time = ScheduleStudent::query()
                ->whereScheduleId($request->schedule_id)
                ->whereStaseId($request->stase_id)
                ->whereStudentId($request->student_id)
                ->where(function ($query) use ($input) {
                    $query
                        ->orWhereBetween('date_start', [date('Y-m-d', strtotime("+1 day", strtotime($input['date_start']))), $input['date_end']])
                        ->orWhereBetween('date_end', [date('Y-m-d', strtotime("+1 day", strtotime($input['date_start']))), $input['date_end']]);
                })
                ->exists();
            if ($check_time) {
                $data['message'] = 'Duplicate Time';
                $status = 500;
                return response()->json($data, $status);
            }

            $check_clash = ScheduleStudent::query()
                ->whereScheduleId($request->schedule_id)
                ->whereStaseId($request->stase_id)
                ->whereStudentId($request->student_id)
                ->whereHospitalId($request->hospital_id)
                ->exists();
            if ($check_clash) {
                $data['message'] = 'Duplicate Bentrok';
                $status = 500;
                return response()->json($data, $status);
            }

            $quota = Quota::with('hospital')
                ->whereHospitalId($request->hospital_id)
                ->whereStaseId($stase->id)
                ->first();
            $count_student_hospital = ScheduleStudent::query()
                ->whereScheduleId($request->schedule_id)
                ->whereHospitalId($request->hospital_id)
                ->whereStaseId($stase->id)
                ->count();

            $check_quota = true;
            if (!empty($quota)) {
                $remaining_quota = $quota->quota - $count_student_hospital;
                if ($remaining_quota > 0) {
                    $check_quota = false;
                }
            }
            if ($check_quota) {
                $data['message'] = 'Quota';
                $status = 500;
                return response()->json($data, $status);
            }

            $data_student_group = [
                'schedule_id' => $request->schedule_id,
                'name' => '100',
            ];
            $schedule_student_group = ScheduleStudentGroup::create($data_student_group);
            $data_student_group_detail = [
                'schedule_student_group_id' => $schedule_student_group->id,
                'student_id' => $request->student_id
            ];
            ScheduleStudentGroupDetail::create($data_student_group_detail);

            $store = ScheduleStudent::create($input);
            if ($store) {
                $data['message'] = 'Sukses tambah data';
                $status = 200;
            } else {
                $data['message'] = 'Data gagal disimpan';
                $status = 500;
            }
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }
}
