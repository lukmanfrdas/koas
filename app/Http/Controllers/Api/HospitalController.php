<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Hospital;
use Illuminate\Http\Request;

class HospitalController extends Controller
{
    public function index() {
        try {
            $hospitals = Hospital::all();
            return response()->json($hospitals);
        } catch (\Exception $ex) {
            return response(400)->json($ex->getMessage());
        }
    }
}
