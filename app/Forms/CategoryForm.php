<?php

namespace App\Forms;

use App\Models\Category;
use Kris\LaravelFormBuilder\Form;

class CategoryForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('parent_id', 'choice', [
                'choices' => $this->getId(),
                'empty_value' => 'Pilih ID',
                'label' => 'ID *', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('name', 'text')
            ->add('image', 'text')
            ->add('status', 'choice', [
                'choices' => [
                    1 => 'Aktif',
                    9 => 'Tidak Aktif'
                ],
                'empty_value' => 'Pilih Status',
                'label' => 'Status', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('submit', 'submit', ['label'=>'Submit', 'attr'=>['class'=>'btn btn-success']]);
    }

    public function getId()
    {
        $id = Category::whereNull('parent_id')->get();
        // dd($id);
    }
}
