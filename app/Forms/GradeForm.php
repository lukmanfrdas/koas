<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class GradeForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('name', 'text', [
            'label' => trans('Angkatan')
        ])
        ->add('submit', 'submit', ['label' => 'Simpan', 'attr' => ['class' => 'btn btn-success']]);
    }
}
