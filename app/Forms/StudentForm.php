<?php

namespace App\Forms;

use App\Models\Grade;
use Kris\LaravelFormBuilder\Form;

class StudentForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('nim', 'text', [
            'label' => trans('NIM')
        ])
        ->add('grade_id', 'choice', [
            'choices' => $this->getGrades(),
            'empty_value' => 'Pilih Angkatan',
            'label' => 'Angkatan *', 'attr' => ['class' => 'form-control select2']
        ])
        ->add('name', 'text', [
            'label' => trans('Nama')
        ])

        ->add('gender', 'choice', [
            'choices' => [
                'Laki - laki' => 'Laki - laki',
                'Perempuan' => 'Perempuan'
            ],
            'empty_value' => 'Pilih Jenis Kelamin',
            'label' => 'Jenis Kelamin', 'attr' => ['class' => 'form-control select2']
        ])

        ->add('birth_date', 'date', [
            'label' => trans('Tanggal Lahir')
        ])

        ->add('status', 'choice', [
            'choices' => [
                'aktif' => 'aktif',
                'nonaktif' => 'nonaktif'
            ],
            'empty_value' => 'Pilih Status',
            'label' => 'Status', 'attr' => ['class' => 'form-control select2']
        ])

        ->add('email', 'email', [
            'label' => trans('Email')
        ])

        ->add('phone', 'number', [
            'label' => trans('Nomor Telepon')
        ])

        ->add('submit', 'submit', ['label' => 'Simpan', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getGrades()
    {
        $grades = Grade::get();
        $data = [];
        foreach ($grades as $grade) {
            $data[$grade->id] = $grade->name;
        }
        return $data;
    }
}
