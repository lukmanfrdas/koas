<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ScheduleForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('code', 'text', [
            'label' => trans('Code')
        ])
        ->add('name', 'text', [
            'label' => trans('Nama')
        ])
        ->add('description', 'textarea', [
            'label' => trans('Deskripsi')
        ])
        ->add('date_start', 'date', [
            'label' => trans('Tanggal Mulai')
        ])
        ->add('date_end', 'date', [
            'label' => trans('Tanggal Berakhir')
        ])
        ->add('submit', 'submit', ['label' => 'Simpan', 'attr' => ['class' => 'btn btn-success']]);
    }
}
