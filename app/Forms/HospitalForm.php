<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class HospitalForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('name', 'text', [
            'label' => trans('Nama')
        ])

        ->add('color', 'color', [
            'label' => trans('Color')
        ])
        ->add('address', 'textarea', [
            'label' => trans('Alamat')
        ])
        ->add('sequence', 'number', [
            'label' => trans('Urutan')
        ])
        ->add('submit', 'submit', ['label' => 'Simpan', 'attr' => ['class' => 'btn btn-success']]);
    }
}
