<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class LevelForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('name', 'text', [
            'label' => trans('Semester')
        ])
        ->add('submit', 'submit', ['label' => 'Simpan', 'attr' => ['class' => 'btn btn-success']]);
    }
}
