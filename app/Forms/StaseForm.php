<?php

namespace App\Forms;

use App\Models\Level;
use Kris\LaravelFormBuilder\Form;

class StaseForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => trans('Nama')
            ])
            ->add('week', 'number', [
                'label' => trans('Minggu')
            ])
            ->add('description', 'textarea', [
                'label' => trans('Deskripsi')
            ])
            ->add('score', 'number', [
                'label' => trans('Nilai Lulus')
            ])
            ->add('level_id', 'choice', [
                'choices' => $this->getLevels(),
                'empty_value' => 'Pilih Semester',
                'label' => 'Semester *', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('submit', 'submit', ['label' => 'Simpan', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getLevels()
    {
        $levels = Level::get();
        $data = [];
        foreach ($levels as $level) {
            $data[$level->id] = $level->name;
        }
        return $data;
    }
}
