<?php

namespace App\Forms;

use App\Models\Stase;
use Kris\LaravelFormBuilder\Form;

class QuotaForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('stase_id', 'choice', [
                'choices' => $this->getStase(),
                'empty_value' => 'Pilih Stase',
                'label' => 'Stase', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('quota', 'number', ['label' => 'Kuota'])
            ->add('submit', 'submit', ['label'=>'Submit', 'attr'=>['class'=>'btn btn-success', 'type' => 'button' ,'id' => 'btn-add-quota']]);
    }

    public function getStase()
    {
        $stases = Stase::all();
        $data = [];
        foreach ($stases as $stase) {
            $data[$stase->id] = $stase->name;
        }
        return $data;
    }
}
