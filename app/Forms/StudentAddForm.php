<?php

namespace App\Forms;

use App\Models\Hospital;
use App\Models\Stase;
use App\Models\Student;
use Kris\LaravelFormBuilder\Form;

class StudentAddForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('student_id', 'choice', [
                'choices' => $this->getStudent(),
                'empty_value' => 'Pilih Student',
                'label' => 'Student', 'attr' => ['class' => 'form-control']
            ])
            ->add('hospital_id', 'choice', [
                'choices' => $this->getHospital(),
                'empty_value' => 'Pilih Hospital',
                'label' => 'Hospital', 'attr' => ['class' => 'form-control']
            ])
            ->add('stase_id', 'choice', [
                'choices' => $this->getStase(),
                'empty_value' => 'Pilih Stase',
                'label' => 'Stase', 'attr' => ['class' => 'form-control']
            ])
            ->add('date_start', 'text', ['label' => 'Tanggal Mulai dengan format (YYYY-MM-DD )', 'placeholder' => 'YYYY-mm-dd']);
    }

    public function getStudent()
    {
        $students = Student::orderBy('name')->get();
        $data = [];
        foreach ($students as $student) {
            $data[$student->id] = $student->name;
        }
        return $data;
    }

    public function getHospital()
    {
        $hospitals = Hospital::get();
        $data = [];
        foreach ($hospitals as $hospital) {
            $data[$hospital->id] = $hospital->name;
        }
        return $data;
    }

    public function getStase()
    {
        $stases = Stase::get();
        $data = [];
        foreach ($stases as $stase) {
            $data[$stase->id] = $stase->name;
        }
        return $data;
    }
}
