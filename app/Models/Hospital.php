<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hospital extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'name',
        'address',
        'color',
        'sequence',
    ];

    public function getLabels() {
        return [
            'name' => 'nama',
            'address' => 'alamat',
            'color' => 'warna',
            'sequence' => 'urutan'
        ];
    }

    public function quotas() {
        return $this->hasMany(Quota::class);
    }

    public function schedule_students() {
        return $this->hasMany(ScheduleStudent::class);
    }
}
