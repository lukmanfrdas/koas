<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grade extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
    ];

    public function students(){
        return $this->hasMany(Student::class);
    }
}
