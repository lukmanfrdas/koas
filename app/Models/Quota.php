<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quota extends Model
{
    use HasFactory;
    protected $fillable = [
        'hospital_id',
        'stase_id',
        'quota'
    ];

    public function stase() {
        return $this->belongsTo(Stase::class);
    }

    public function hospital() {
        return $this->belongsTo(Hospital::class);
    }

}
