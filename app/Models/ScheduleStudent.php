<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleStudent extends Model
{
    use HasFactory;
    protected $fillable = [
        'schedule_id',
        'student_id',
        'stase_id',
        'hospital_id',
        'date_start',
        'date_end',
        'score'
    ];

    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function stase()
    {
        return $this->belongsTo(Stase::class);
    }

    public function hospital()
    {
        return $this->belongsTo(Hospital::class);
    }


}
