<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleStudentGroupDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'schedule_student_group_id',
        'student_id',
    ];

    public function schedule_student_group()
    {
        return $this->belongsTo(ScheduleStudentGroup::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
