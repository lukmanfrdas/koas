<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'nim',
        'name',
        'gender',
        'birth_date',
        'grade_id',
        'status',
        'email',
        'phone'
    ];

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    public function schedule_students()
    {
        return $this->hasMany(ScheduleStudent::class, 'student_id', 'id');
    }
}
