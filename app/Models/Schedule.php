<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use HasFactory;
    protected $fillable = [
        'code',
        'name',
        'level_id',
        'description',
        'date_start',
        'date_end',
    ];

    public function schedule_student_groups()
    {
        return $this->hasMany(ScheduleStudentGroup::class);
    }

    public function schedule_students()
    {
        return $this->hasMany(ScheduleStudent::class);
    }

    public function level(){
        return $this->belongsTo(level::class);
    }
}
