<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Level extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
    ];

    public function stases(){
        return $this->hasMany(Stase::class);
    }

    public function schedules(){
        return $this->hasMany(Schedule::class);
    }
}
