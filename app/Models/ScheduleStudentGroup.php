<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleStudentGroup extends Model
{
    use HasFactory;
    protected $fillable = [
        'schedule_id',
        'name',

    ];

    public function schedule_student_group_details()
    {
        return $this->hasMany(ScheduleStudentGroupDetail::class);
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }
}
