<?php

use App\Http\Controllers\Api\ScheduleStudentController;
use App\Http\Controllers\Auth\GoogleController;
use App\Http\Controllers\BalanceController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\QuotaController;
use App\Http\Controllers\StaseController;
use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('auth/google', [GoogleController::class, 'redirectToGoogle'])->name('auth.google');
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);

Route::group(['middleware' => ['auth']], function () {
    Route::get('', [HomeController::class, 'index'])->name('home');
    Route::get('home/report', [HomeController::class, 'report'])->name('home.report');

    Route::patch('user/login-as/{id}', [UserController::class, 'loginAs'])->name('user.login-as');
    Route::get('report', [HomeController::class, 'report'])->name('home.report');
    Route::resource('user', UserController::class);
    Route::resource('permission', PermissionController::class);
    Route::resource('role', RoleController::class);
    Route::resource('student', StudentController::class);
    Route::resource('hospital', HospitalController::class);
    Route::resource('stase', StaseController::class);
    Route::resource('schedule', ScheduleController::class);
    Route::resource('level', LevelController::class);
    Route::resource('grade', GradeController::class);
    Route::post('schedule/select-student', [ScheduleController::class, 'select_student'])->name('schedule.select-student');
    Route::post('schedule/update-schedule-student/{id}', [ScheduleController::class, 'update_schedule_student'])->name('schedule.update-schedule-student');
    Route::get('schedule/{id}/data', [ScheduleController::class, 'data'])->name('schedule.data');
    Route::post('schedule/{id}/drag', [ScheduleController::class, 'drag'])->name('schedule.drag');
    Route::get('schedule/{id}/report', [ScheduleController::class, 'report'])->name('schedule.report');
    Route::get('schedule/{schedule_id}/report/{student_id}', [ScheduleController::class, 'reportDetail'])->name('schedule.report-detail');
    Route::get('schedule/{id}/fullscreen', [ScheduleController::class, 'fullscreen'])->name('schedule.fullscreen');

    Route::resource('quota', QuotaController::class);
    Route::post('students/import', [StudentController::class, 'import'])->name('students.import');

    Route::get('stase/{id}/student-list', [StaseController::class, 'studentList'])->name('stase.student-list');
    Route::get('level/schedule/{id}/student-list', [LevelController::class, 'studentList'])->name('level.student-list');
    Route::get('students/export', [StudentController::class, 'export'])->name('students.export');
    Route::get('schedules/export/{id}', [ScheduleController::class, 'export'])->name('schedules.export');

    //api
    Route::get('api/hospital', [HospitalController::class, 'index'])->name('api.hospital.index');
    Route::get('api/stase', [StaseController::class, 'index'])->name('api.stase.index');
    Route::post('api/schedule-student/store', [ScheduleStudentController::class, 'store'])->name('api.schedule-student.store');
    Route::put('api/schedule-student/store-new-student', [ScheduleStudentController::class, 'storeNewStudent'])->name('api.schedule-student.store-new-student');
    Route::delete('api/schedule-student/{id}', [ScheduleStudentController::class, 'destroy'])->name('api.schedule-student.destroy');
    Route::put('api/schedule-student/{id}', [ScheduleStudentController::class, 'update'])->name('api.schedule-student.update');
});

Route::get('check-time', function () {
    return date('Y-m-d H:i:s');
});
